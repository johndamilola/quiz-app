/**
    main.scala
    Member-function definitions for class Polling that uses a two-dimensional array to store ratings.
    author: John Damilola <johndamilola03@gmail.com>
    26/04/17
 */

import scala.io.StdIn

object PollingApp {
    var issues: List[String] = List()

    case class Data(topics: String, count: Int, average:Double)

    var ratings: List[Data] = List(Data("sddsdg", 0, 0))

    def main(args: Array[String]): Unit = {
        println("Enter 1 default topics or 2 for custom topics")
        println(setTopics())
        println(setData())
    }

    def defaultTopics = () => List("Web Developers", "Programmers", "Data Science", "Security Expert")

    // function to set Topics
    def setTopics():List[String] = StdIn.readInt() match {
            case 1 => defaultTopics();
            case 2 => enterTopics()
            case _ => List()
        }
        
    def enterTopics():List[String] = {
        var reply: Boolean = true 
        while (reply) {
            println("Enter the topics you want to poll against")
            val topic: String = StdIn.readLine();
            if (topic == "" || topic == null) reply = false
            else issues = topic :: issues
        }
       issues
    }

    def setData():List[Data] = {
        for (topic <- setTopics())
            ratings = Data(topic, 0, 0) :: ratings
        ratings
    }
        

}